#pragma once
#include "Ball.h"
#include "Paddle.h"

class GameSystem
{
public:
	int player1Score = 0;
	int player2Score = 0;
	bool roundFinished = false;
	bool gameOver = false;

	void handleInput(Paddle &player1, Paddle &player2, sf::Vector2u windowSize);
	bool ballIsOutOfBounds(Ball &ball, sf::Vector2u windowSize);

private:
	const float paddleVelocity = 6.5f;
};