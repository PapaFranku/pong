#pragma once
#include <SFML/Graphics.hpp>

class Paddle
{
public:
	Paddle(sf::Vector2f size);

	void setPos(sf::Vector2f newPos);
	sf::Vector2f getPosition() const;
	sf::Vector2f getSize() const;
	sf::FloatRect getGlobalBounds() const;
	void drawTo(sf::RenderWindow &window);
	void move(sf::Vector2f distance);
private:
	sf::RectangleShape paddle;
};