#pragma once
#include <SFML/Graphics.hpp>
#include "Paddle.h"

class Ball
{
public:
	Ball(float radius);

	void setPos(sf::Vector2f newPos);
	sf::Vector2f getPosition() const;
	void drawTo(sf::RenderWindow &window);
	void move();
	void collision(Paddle &paddle, Paddle &paddle2, sf::Vector2u windowSize);

private:
	sf::CircleShape ball;
	sf::Vector2f ballSpeed = { 7.5f, 4.5f };
};