#include "GameSystem.h"

void GameSystem::handleInput(Paddle &player1, Paddle &player2, sf::Vector2u windowSize)
{
	//Player 1
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		if (player1.getPosition().y >= 10)
		{
			player1.move({ 0, -paddleVelocity });
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		if (player1.getPosition().y + 90 <= windowSize.y - 10)
		{
			player1.move({ 0, paddleVelocity });
		}
	}

	//Player 2
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		if (player2.getPosition().y >= 10)
		{
			player2.move({ 0, -paddleVelocity });
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		if (player2.getPosition().y + 90 <= windowSize.y - 10)
		{
			player2.move({ 0, paddleVelocity });
		}
	}
}

bool GameSystem::ballIsOutOfBounds(Ball &ball, sf::Vector2u windowSize)
{
	if (ball.getPosition().x > windowSize.x)
	{
		player1Score++;
		roundFinished = true;

		return true;
	}
	else if (ball.getPosition().x < -16)
	{
		player2Score++;
		roundFinished = true;

		return true;
	}
	else
	{
		return false;
	}
}