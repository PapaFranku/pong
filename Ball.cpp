#include "Ball.h"
#include <iostream>

Ball::Ball(float radius)
{
	ball.setRadius(radius);
	ball.setFillColor(sf::Color::White);
}

void Ball::setPos(sf::Vector2f newPos)
{
	ball.setPosition(newPos);
}

sf::Vector2f Ball::getPosition() const
{
	return ball.getPosition();
}

void Ball::drawTo(sf::RenderWindow &window)
{
	window.draw(ball);
}

void Ball::move()
{
	ball.move(ballSpeed);
}

void Ball::collision(Paddle &paddle, Paddle &paddle2, sf::Vector2u windowSize)
{
	if (ball.getPosition().y <= 0 || ball.getPosition().y >= windowSize.y - 20)
	{
		ballSpeed.y *= -1;
	}
	else if (ball.getGlobalBounds().intersects(paddle.getGlobalBounds()) || 
				ball.getGlobalBounds().intersects(paddle2.getGlobalBounds()))
	{
		ballSpeed.x *= -1;
	}
}