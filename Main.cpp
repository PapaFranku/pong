#include "Ball.h"
#include "Paddle.h"
#include "GameSystem.h"
#include <sstream>


int main()
{
	sf::RenderWindow window;

	window.create(sf::VideoMode(850, 550), "Pong", sf::Style::Titlebar | sf::Style::Close);
	window.setFramerateLimit(60);

	//Initialize the game system and clock
	GameSystem system;
	sf::Clock clock;

	//Initialize objects
	Paddle player1({ 20.f, 90.f });
	player1.setPos({10.f, 230.f});

	Paddle player2({ 20.f, 90.f });
	player2.setPos({ 820.f, 230.f });

	Ball ball(8.5f);
	ball.setPos({416.5f, 266.5f});

	//Import font
	sf::Font arial;
	arial.loadFromFile("arial.ttf");
	std::ostringstream ssScore;
	ssScore << "0 | 0";

	//Initialize labels
	sf::Text lblScore;
	lblScore.setCharacterSize(30);
	lblScore.setPosition({ 395, 10 });
	lblScore.setFont(arial);
	lblScore.setString(ssScore.str());

	sf::Text gameOverText;
	gameOverText.setCharacterSize(30);
	gameOverText.setPosition({ 290, 100 });
	gameOverText.setFont(arial);

	while (window.isOpen())
	{
		//Window closed event
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::EventType::Closed)
			{
				window.close();
			}
		}

		if (system.gameOver == false)
		{
			//Player movement
			system.handleInput(player1, player2, window.getSize());

			if (system.roundFinished == false)
			{
				//Action
				ball.move();
				ball.collision(player1, player2, window.getSize());

				if (system.ballIsOutOfBounds(ball, window.getSize()))
				{
					if (system.player1Score == 10)
					{
						gameOverText.setString("Player 1 Wins!\nPress enter to restart...");
						system.roundFinished = false;
						system.gameOver = true;
					}
					else if (system.player2Score == 10)
					{
						gameOverText.setString("Player 2 Wins!\nPress enter to restart...");
						system.roundFinished = false;
						system.gameOver = true;
					}

					ssScore.str("");
					ssScore << system.player1Score << " | " << system.player2Score;
					lblScore.setString(ssScore.str());
					clock.restart();
				}
			}
			else
			{
				//Wait some time before next round
				if (clock.getElapsedTime() > sf::seconds(1.f))
				{
					system.roundFinished = false;
					ball.setPos({ 416.5f, 266.5f });
				}
			}

			//Update
			window.clear();
			ball.drawTo(window);
			player1.drawTo(window);
			player2.drawTo(window);
			window.draw(lblScore);
			window.display();
		}
		else
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				system.gameOver = false;
				ssScore.str("");
				ssScore << "0 | 0";
				lblScore.setString(ssScore.str());
				ball.setPos({ 416.5f, 266.5f });
				system.player1Score = 0;
				system.player2Score = 0;
			}

			//Update
			window.clear();
			window.draw(gameOverText);
			window.display();
		}
	}

	return 0;
}