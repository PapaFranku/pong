#include "Paddle.h"

Paddle::Paddle(sf::Vector2f size)
{
	paddle.setSize(size);
	paddle.setFillColor(sf::Color::White);
}

void Paddle::setPos(sf::Vector2f newPos)
{
	paddle.setPosition(newPos);
}

sf::Vector2f Paddle::getPosition() const
{
	return paddle.getPosition();
}

sf::Vector2f Paddle::getSize() const
{
	return paddle.getSize();
}

sf::FloatRect Paddle::getGlobalBounds() const
{
	return paddle.getGlobalBounds();
}

void Paddle::drawTo(sf::RenderWindow &window)
{
	window.draw(paddle);
}

void Paddle::move(sf::Vector2f distance)
{
	paddle.move(distance);
}